﻿using System;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            //var declaration 
            var x = 2.12;
            var y = 3.43;
            var result = x + y;
            System.Console.WriteLine($"The First sum is {result:N1}");

            //array declaration
            var number = new[] { 5, 6, 4, 2, 6 };
            var result1 = 0;
            foreach (var numbers in number)
            {
                result1 += numbers;
            }
            System.Console.WriteLine($"The Second sum is {result1}");

            //class defination
            var book = new Book("Rijvi's grade book");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.5);
            var starts=book.GetStatitics();
            System.Console.WriteLine($"The Average valu is {starts.Average:N1}");
            System.Console.WriteLine($"The Lower valu is {starts.Low}");
            System.Console.WriteLine($"The Higher valu is {starts.High}");

            //debugging 
            if (args.Length > 0)
            {
                Console.WriteLine("Hello " + args[0] + "!");//string concatenation
                Console.WriteLine($"Hello, {args[0]}!");//string interpolation


            }
            else
            {
                Console.WriteLine("Hello!");
                System.Console.WriteLine();//type cw for shortcut of print




            }
        }
    }
}