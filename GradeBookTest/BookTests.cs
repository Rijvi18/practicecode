using System;
using Xunit;

namespace GradeBook.Test
{
    public class BookTests
    {
        [Fact]
        public void Test1()
        {
            //arrange
            var book = new Book("");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.5);
            //act
            var result = book.GetStatitics();
            //assert
            Assert.Equal(85.7, result.Average,1);
            Assert.Equal(90.5, result.High,1); 
            Assert.Equal(77.5, result.Low,1);


        }
        [Fact]
        public void Test2()
        {
            //arrange
            var book = new Book("");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.5);
            //act
            var result = book.GetStatitics();
            //assert
            Assert.Equal(85.7, result.Average, 1);
            Assert.Equal(90.5, result.High, 1);
            Assert.Equal(77.5, result.Low, 1);


        }

    }
}
