﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;
using Xunit;

namespace GradeBook.Test
{
    public class Typetests
    {
        [Fact]
        public void ValueTypePassByValue()
        {
            var x = GetInt();
            SetInt(ref x);
            Assert.Equal(42, x);

        }

        private void SetInt(ref int z)
        {
            z = 42;
        }

        private int GetInt()
        {
            return 3 ;
        }


        [Fact]
        public void StringBehaveLikevalueType()
        {
            string name = "Rijvi";
            var upper = MakeUppercase(name);

            Assert.Equal("Rijvi", name);
            Assert.Equal("RIJVI", upper);

        }

        private string MakeUppercase(string parameter)
        {
            return parameter.ToUpper();
        }



        [Fact]
        public void CsharpeCanPassByReference()
        {
            var book1 = GetBook("Book 1");
            GetBookSetName(out book1, "New Name");

            Assert.Equal("New Name", book1.Name);


        }
        private void GetBookSetName(out Book book, string name)
        {
            book = new Book(name);
        }


        [Fact]
        public void CshrapePassByValue()
        {
            var book1 = GetBook("Book 1");
            GetBookSetName(book1, "New name");

            Assert.Equal("Book 1", book1.Name);


        }
        private void GetBookSetName(Book book, string name)
        {
            book = new Book(name);
        }


        [Fact]
        public void CanSetNameFromReference()
        {
            var book1 = GetBook("Book 1");
            SetName(book1, "New  Name");

            Assert.Equal("New  Name", book1.Name);
          
           
        }

        private void SetName(Book book, string name)
        {
            book.Name = name;
        }


        [Fact]
        public void GetBookReturnDiffObj()
        {
            var book1 = GetBook("Book 1");
            var book2 = GetBook("Book 2");

            Assert.Equal("Book 1", book1.Name);
            Assert.Equal("Book 2", book2.Name);
            Assert.NotSame(book1, book2);
        }


        [Fact]
        public void TwoVarsCanReferenceSameObj()
        {
            var book1 = GetBook("Book 1");
            var book2 = book1;

            Assert.Same(book1,book2);
            Assert.True(Object.ReferenceEquals(book1,book2));
        }

        Book GetBook(string name)
        {
            return new Book(name);
        }
    }
}
